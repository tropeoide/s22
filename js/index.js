      $(function(){
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
            interval: 1000  
        });
        $('#contactoBtn').on('show.bs.modal', function (e){
          console.log('el modal se está mostrando');
          /*cuando se abre el modal cambia el color para indicar que ya lo ha visitado*/
          $('#contactoBtn').removeClass('btn-outline-success');
          $('#contactoBtn').addClass('btn-primary');
          $('#contactoBtn').prop('disable', true);
        })
        $('#contactoBtn').on('shown.bs.modal', function (e){
          console.log('el modal contacto se mostró');
        })
        $('#contactoBtn').on('hide.bs.modal', function (e){
          console.log('el modal se oculta');
        })
        $('#contactoBtn').on('hidden.bs.modal', function (e){
          console.log('el modal contacto ocultó');
          $('#contactoBtn').prop('disable', false);
        })
      });